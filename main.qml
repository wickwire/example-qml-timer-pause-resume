import QtQuick 2.1
import QtQuick.Window 2.0

Window {
    visible: true
    width: 360
    height: 360

    property string initialTimestamp
    property string currentTimestamp
    property string timerTimestamp
    property int timerNormalInterval : 5000
    property int timerAfterPauseInterval

    MouseArea {
        anchors.fill: parent
        onClicked: {
            //Qt.quit();
            currentTimestamp=new Date().getTime().toString();

            if(controlledTimer.running==true){

                controlledTimer.stop();
                if(!timerTimestamp){
                    timerTimestamp=initialTimestamp;
                }

                console.log(controlledTimer.interval + "!" + timerTimestamp + "!" + currentTimestamp);

                timerAfterPauseInterval=controlledTimer.interval-(currentTimestamp-timerTimestamp)
            }
            else{
                controlledTimer.interval=timerAfterPauseInterval
                controlledTimer.restart();
            }
            console.log("timerTimestamp: " + timerTimestamp + " vs currentTimestamp: " + currentTimestamp + " timer already used: " +(currentTimestamp-timerTimestamp) + " timer left to use: " + timerAfterPauseInterval);
        }
    }

    Timer{
        id: controlledTimer
        running: true
        repeat: true
        interval: timerNormalInterval
        onTriggered:{
            timerTimestamp=new Date().getTime().toString();
            console.log("Timer Fired: currentTimestamp: " + currentTimestamp + " timerTimestamp: " + timerTimestamp + " timer duration: " + controlledTimer.interval);
            if(interval != timerNormalInterval){
                interval=timerNormalInterval
            }
        }
    }

    Text {
        id: cenasTxt
        text: qsTr("Hello World")
        anchors.centerIn: parent
    }

    Component.onCompleted: {
        initialTimestamp = new Date().getTime().toString();
    }
}
